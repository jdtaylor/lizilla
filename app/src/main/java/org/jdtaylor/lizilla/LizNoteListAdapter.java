package org.jdtaylor.lizilla;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

public class LizNoteListAdapter extends ListAdapter<LizNote, LizNoteViewHolder> {

    public LizNoteListAdapter(@NonNull DiffUtil.ItemCallback<LizNote> diffCallback) {
        super(diffCallback);
    }

    @Override
    public LizNoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return LizNoteViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(LizNoteViewHolder holder, int position) {
        LizNote current = getItem(position);
        holder.bind(current.note + ",\t\t" + current.timestamp);
    }

    static class WordDiff extends DiffUtil.ItemCallback<LizNote> {

        @Override
        public boolean areItemsTheSame(@NonNull LizNote oldItem, @NonNull LizNote newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull LizNote oldItem, @NonNull LizNote newItem) {
            return oldItem.note.equals(newItem.note);
        }
    }
}