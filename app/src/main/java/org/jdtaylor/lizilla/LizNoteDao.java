package org.jdtaylor.lizilla;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LizNoteDao {
    @Query("SELECT * FROM liz_notes")
    LiveData<List<LizNote>> getAll();

    // TODO: jdt: incorrect, use @Fts4
    @Query("SELECT * FROM liz_notes WHERE note LIKE :searchString LIMIT 1")
    LizNote findByNote(String searchString);

    @Query("DELETE FROM liz_notes")
    void deleteAll();

    @Insert
    void insertAll(LizNote... note);

    @Delete
    void delete(LizNote note);
}
