package org.jdtaylor.lizilla;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class LizNoteViewModel extends AndroidViewModel {

   private LizNoteRepository mRepository;

   private final LiveData<List<LizNote>> mAllNotes;

   public LizNoteViewModel (Application application) {
       super(application);
       mRepository = new LizNoteRepository(application);
       mAllNotes = mRepository.getAllNotes();
   }

   LiveData<List<LizNote>> getAllNotes() { return mAllNotes; }

    void deleteAllNotes() { mRepository.deleteAllNotes(); }

   public void insert(LizNote note) { mRepository.insert(note); }
}