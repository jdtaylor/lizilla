package org.jdtaylor.lizilla;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Fts4;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "liz_notes")
public class LizNote {
    // jdt: Room entities require at least one column be 'primarykey' even though
    //      sqlite has rowid if not excluded.. department of redundancy department:
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uid")
    public int uid;

    public LizNote() {
        note = "";
    }

    public LizNote(String mynote) {
        note = mynote;
    }

    public LizNote(String time, String mynote) {
        timestamp = time;
        note = mynote;
    }

    // ISO-8601 w/ millisecond precision and UTF via /Z
    @ColumnInfo(name = "timestamp")
    public String timestamp;

    @ColumnInfo(name = "note")
    public String note;
}
