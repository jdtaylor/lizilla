package org.jdtaylor.lizilla;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

class LizNoteViewHolder extends RecyclerView.ViewHolder {
    private final TextView noteItemView;

    private LizNoteViewHolder(View itemView) {
        super(itemView);
        noteItemView = itemView.findViewById(R.id.textView);
    }

    public void bind(String text) {
        noteItemView.setText(text);
    }

    static LizNoteViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notesview_item, parent, false);
        return new LizNoteViewHolder(view);
    }
}