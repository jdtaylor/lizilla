package org.jdtaylor.lizilla;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

public class NewLizNoteActivity extends AppCompatActivity {

    public static final String EXTRA_TIME = "org.jdtaylor.lizilla.REPLY_TIME";
    public static final String EXTRA_NOTE = "org.jdtaylor.lizilla.REPLY_NOTE";

    private EditText mEditNoteView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_liz_note);
        mEditNoteView = findViewById(R.id.edit_note);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            if (TextUtils.isEmpty(mEditNoteView.getText())) {
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                Instant instant = Instant.now();
                String tm = instant.toString();
                String word = mEditNoteView.getText().toString();
                replyIntent.putExtra(EXTRA_NOTE, word);
                replyIntent.putExtra(EXTRA_TIME, tm);
                setResult(RESULT_OK, replyIntent);
            }
            finish();
        });
    }
}