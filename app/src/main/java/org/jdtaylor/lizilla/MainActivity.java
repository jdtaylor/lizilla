package org.jdtaylor.lizilla;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int NEW_NOTE_ACTIVITY_REQUEST_CODE = 1;

    private LizNoteViewModel mLizNoteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final LizNoteListAdapter adapter = new LizNoteListAdapter(new LizNoteListAdapter.WordDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mLizNoteViewModel = new ViewModelProvider(this).get(LizNoteViewModel.class);
        mLizNoteViewModel.getAllNotes().observe(this, notes -> {
            // Update the cached copy of the notes in the adapter.
            adapter.submitList(notes);
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener( view -> {
           Intent intent = new Intent(MainActivity.this, NewLizNoteActivity.class);
           startActivityForResult(intent, NEW_NOTE_ACTIVITY_REQUEST_CODE);
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_NOTE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            LizNote word = new LizNote(data.getStringExtra(NewLizNoteActivity.EXTRA_TIME),
                    data.getStringExtra(NewLizNoteActivity.EXTRA_NOTE));
            mLizNoteViewModel.insert(word);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.liz_note_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.clear_all_notes:
                clearAllNotes();
                return true;
            case R.id.export_csv:
                exportCSV();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void clearAllNotes() {
        Log.d("Menu Chosen", "Clear All Notes");
        // TODO: ask to verify
        mLizNoteViewModel.deleteAllNotes();
    }

    public void exportCSV() {
        Log.d("Menu Chosen", "Export CSV");
    }

    private void ShareViaEmail(String folder_name, String file_name) {
        try {
            File root = Environment.getExternalStorageDirectory();
            String fileLocation = root.getAbsolutePath() + folder_name + "/" + file_name;
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setType("text/plain");
            String message = "File to be shared is " + file_name + ".";
            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + fileLocation));
            intent.putExtra(Intent.EXTRA_TEXT, message);
            // TODO: TODO TODO!!!
            intent.setData(Uri.parse("mailto:james.d.taylor@gmail.com"));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);
        } catch (Exception e) {
            System.out.println("is exception raises during sending mail" + e);
        }
    }
}

}